#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: Client
#------------------------------------------------------------

CREATE TABLE Client(
        iid_client       Int  Auto_increment  NOT NULL ,
        ctel_client      Varchar (15) ,
        cmel_client      Varchar (50) ,
        cAddresse_client Varchar (50) ,
        ccp_client       Varchar (10) ,
        cville_client    Varchar (50) ,
        cnom_client      Varchar (50) NOT NULL
	,CONSTRAINT Client_AK UNIQUE (cnom_client)
	,CONSTRAINT Client_PK PRIMARY KEY (iid_client)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: TypeAnalyse
#------------------------------------------------------------

CREATE TABLE TypeAnalyse(
        iid_type_analyse   Int  Auto_increment  NOT NULL ,
        ccode_type_analyse Varchar (10) NOT NULL ,
        cnom_type_analyse  Varchar (50) NOT NULL
	,CONSTRAINT TypeAnalyse_AK UNIQUE (ccode_type_analyse,cnom_type_analyse)
	,CONSTRAINT TypeAnalyse_PK PRIMARY KEY (iid_type_analyse)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Operation
#------------------------------------------------------------

CREATE TABLE Operation(
        iid_operation Int  Auto_increment  NOT NULL ,
        cdescription  Text ,
        cnom_etape    Varchar (50) NOT NULL
	,CONSTRAINT Operation_AK UNIQUE (cnom_etape)
	,CONSTRAINT Operation_PK PRIMARY KEY (iid_operation)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Role
#------------------------------------------------------------

CREATE TABLE Role(
        iid_role  Int  Auto_increment  NOT NULL ,
        cnom_role Varchar (50) NOT NULL
	,CONSTRAINT Role_AK UNIQUE (cnom_role)
	,CONSTRAINT Role_PK PRIMARY KEY (iid_role)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Personnel
#------------------------------------------------------------

CREATE TABLE Personnel(
        iid_personne     Int  Auto_increment  NOT NULL ,
        cnom_personne    Varchar (50) NOT NULL ,
        cprenom_personne Varchar (50) NOT NULL ,
        ctel_personne    Varchar (50) ,
        cmel_personne    Varchar (50) NOT NULL ,
        iid_role         Int NOT NULL
	,CONSTRAINT Personnel_PK PRIMARY KEY (iid_personne)

	,CONSTRAINT Personnel_Role_FK FOREIGN KEY (iid_role) REFERENCES Role(iid_role)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Dossier
#------------------------------------------------------------

CREATE TABLE Dossier(
        iid_dossier         Int  Auto_increment  NOT NULL ,
        darrive_theo        Date NOT NULL ,
        darrive_reel        Date ,
        inombre_echantillon Int NOT NULL ,
        iordre_priorite     Int NOT NULL ,
        cnum_dossier        Varchar (10) NOT NULL ,
        cnom_dossier        Varchar (50) NOT NULL ,
        iid_client          Int NOT NULL ,
        iid_personne        Int NOT NULL
	,CONSTRAINT Dossier_AK UNIQUE (cnum_dossier,cnom_dossier)
	,CONSTRAINT Dossier_PK PRIMARY KEY (iid_dossier)

	,CONSTRAINT Dossier_Client_FK FOREIGN KEY (iid_client) REFERENCES Client(iid_client)
	,CONSTRAINT Dossier_Personnel0_FK FOREIGN KEY (iid_personne) REFERENCES Personnel(iid_personne)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Date
#------------------------------------------------------------

CREATE TABLE Date(
        tHeure_Debut TimeStamp NOT NULL
	,CONSTRAINT Date_PK PRIMARY KEY (tHeure_Debut)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Abaque
#------------------------------------------------------------

CREATE TABLE Abaque(
        iid_abaque       Int  Auto_increment  NOT NULL ,
        inb_echantillons Int NOT NULL ,
        iduree           Int NOT NULL ,
        iid_operation    Int NOT NULL
	,CONSTRAINT Abaque_PK PRIMARY KEY (iid_abaque)

	,CONSTRAINT Abaque_Operation_FK FOREIGN KEY (iid_operation) REFERENCES Operation(iid_operation)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Contenir
#------------------------------------------------------------

CREATE TABLE Contenir(
        iid_type_analyse Int NOT NULL ,
        iid_dossier      Int NOT NULL
	,CONSTRAINT Contenir_PK PRIMARY KEY (iid_type_analyse,iid_dossier)

	,CONSTRAINT Contenir_TypeAnalyse_FK FOREIGN KEY (iid_type_analyse) REFERENCES TypeAnalyse(iid_type_analyse)
	,CONSTRAINT Contenir_Dossier0_FK FOREIGN KEY (iid_dossier) REFERENCES Dossier(iid_dossier)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Inclure
#------------------------------------------------------------

CREATE TABLE Inclure(
        iid_operation    Int NOT NULL ,
        iid_type_analyse Int NOT NULL ,
        inumero          Int NOT NULL ,
        iecart_min       Int NOT NULL ,
        iecart_max       Int NOT NULL
	,CONSTRAINT Inclure_PK PRIMARY KEY (iid_operation,iid_type_analyse)

	,CONSTRAINT Inclure_Operation_FK FOREIGN KEY (iid_operation) REFERENCES Operation(iid_operation)
	,CONSTRAINT Inclure_TypeAnalyse0_FK FOREIGN KEY (iid_type_analyse) REFERENCES TypeAnalyse(iid_type_analyse)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Avoir (compétence)
#------------------------------------------------------------

CREATE TABLE Avoir__competence_(
        iid_personne  Int NOT NULL ,
        iid_operation Int NOT NULL ,
        iniveau       Int NOT NULL
	,CONSTRAINT Avoir__competence__PK PRIMARY KEY (iid_personne,iid_operation)

	,CONSTRAINT Avoir__competence__Personnel_FK FOREIGN KEY (iid_personne) REFERENCES Personnel(iid_personne)
	,CONSTRAINT Avoir__competence__Operation0_FK FOREIGN KEY (iid_operation) REFERENCES Operation(iid_operation)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Realiser
#------------------------------------------------------------

CREATE TABLE Realiser(
        iid_type_analyse Int NOT NULL ,
        iid_dossier      Int NOT NULL ,
        tHeure_Debut     TimeStamp NOT NULL ,
        iid_operation    Int NOT NULL ,
        iid_personne     Int NOT NULL
	,CONSTRAINT Realiser_PK PRIMARY KEY (iid_type_analyse,iid_dossier,tHeure_Debut,iid_operation,iid_personne)

	,CONSTRAINT Realiser_TypeAnalyse_FK FOREIGN KEY (iid_type_analyse) REFERENCES TypeAnalyse(iid_type_analyse)
	,CONSTRAINT Realiser_Dossier0_FK FOREIGN KEY (iid_dossier) REFERENCES Dossier(iid_dossier)
	,CONSTRAINT Realiser_Date1_FK FOREIGN KEY (tHeure_Debut) REFERENCES Date(tHeure_Debut)
	,CONSTRAINT Realiser_Operation2_FK FOREIGN KEY (iid_operation) REFERENCES Operation(iid_operation)
	,CONSTRAINT Realiser_Personnel3_FK FOREIGN KEY (iid_personne) REFERENCES Personnel(iid_personne)
)ENGINE=InnoDB;

