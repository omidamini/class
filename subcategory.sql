DROP TABLE IF EXISTS subcategory;
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE subcategory (
  id int NOT NULL,
  category_id int NOT NULL,
  subcategory_name varchar(50) NOT NULL,
  PRIMARY KEY (id)
)  ;

CREATE INDEX IDX_DDCA44812469DE2 ON subcategory (category_id);

INSERT INTO subcategory (id, category_id, subcategory_name) VALUES
(5, 3, 'Fruits & Vegetables'),
(6, 2, 'Cell Phones & Accessories'),
(8, 34, 'Oral Care'),
(9, 3, 'Deli'),
(10, 34, 'Bath & Body'),
(11, 38, 'Women'),
(12, 38, 'Men'),
(13, 2, 'Tablets & Accessories');


ALTER TABLE subcategory
  ADD CONSTRAINT FK_DDCA44812469DE2 FOREIGN KEY (category_id) REFERENCES category (id);
