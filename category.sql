DROP TABLE IF EXISTS category;
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE category (
  id int NOT NULL ,
  category_name varchar(50) NOT NULL,
  type varchar(15) DEFAULT NULL,
  category_image varchar(255) NOT NULL,
  PRIMARY KEY (id)
)  ;
INSERT INTO category (id, category_name, type, category_image) VALUES
(2, 'Electronics', 'occasional', 'electronics.jpg'),
(3, 'Grocery', 'common', 'grocery.jpg'),
(33, 'Baby & Toddler', 'common', 'baby&toddler.jpg'),
(34, 'Health & Beauty', 'common', 'health&beauty.jpg'),
(35, 'Home & Garden', 'common', 'home&garden.jpg'),
(38, 'Fashion & Accessories', 'occasional', 'fashion&accessories.jpg'),
(40, 'Sports & Leisure', 'occasional', 'sport&leasure.jpg'),
(41, 'Toys & Games', 'occasional', 'toys&games.jpg'),
(42, 'Media', 'occasional', 'media.jpg'),
(43, 'Motors', 'occasional', 'motors.jpg'),
(44, 'Property', 'occasional', 'property.jpg'),
(45, 'Services', 'occasional', 'services.jpg'),
(46, 'Others', 'occasional', 'other.jpg'),
(54, 'Jewellery & watches', 'occasional', 'jewelry-watch-633427dcc2793.jpg'),
(55, 'test', 'occasional', 'logo4x4-632f1cce4925c-63342e7746b96.jpg');
