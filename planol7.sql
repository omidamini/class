


DROP DATABASE projet_elisol;


CREATE DATABASE projet_elisol;


ALTER DATABASE projet_elisol;

connect projet_elisol





DECLARE @CREATE TABLE
 BEGIN abaque (
    iid_abaque
    return null;
    end; integer NOT NULL,
    inb_echantillons integer NOT NULL,
    iduree integer NOT NULL,
    iid_etape integer NOT NULL
);


ALTER TABLE abaque


CREATE SEQUENCE abaque_iid_abaque_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE abaque_iid_abaque_seq



ALTER SEQUENCE abaque_iid_abaque_seq OWNED BY abaque.iid_abaque;


CREATE TABLE client (
    iid_client integer NOT NULL,
    ctel character varying(15),
    cmel character varying(50),
    cadresse character varying(50),
    ccp character varying(10),
    cville character varying(50),
    cnom character varying(50) NOT NULL
);


ALTER TABLE client


CREATE SEQUENCE client_iid_client_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE client_iid_client_seq


ALTER SEQUENCE client_iid_client_seq OWNED BY client.iid_client;

CREATE TABLE competence (
    iid_personne integer NOT NULL,
    iid_etape integer NOT NULL,
    iniveau integer NOT NULL
);


ALTER TABLE competence


CREATE TABLE contenir (
    iid_typeanalyse integer NOT NULL,
    iid_dossier integer NOT NULL
);


ALTER TABLE contenir


CREATE TABLE dossier (
    iid_dossier integer NOT NULL,
    darrive_theo date NOT NULL,
    darrive_reel date,
    inombre_echantillon integer NOT NULL,
    iordre_priorite integer NOT NULL,
    cnum character varying(10) NOT NULL,
    cnom character varying(50) NOT NULL,
    iid_client integer NOT NULL,
    iid_personne integer NOT NULL
);


ALTER TABLE dossier


CREATE SEQUENCE dossier_iid_dossier_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dossier_iid_dossier_seq



ALTER SEQUENCE dossier_iid_dossier_seq OWNED BY dossier.iid_dossier;


CREATE TABLE etape (
    iid_etape integer NOT NULL,
    cdescription character varying(2000),
    cnom character varying(50) NOT NULL
);


ALTER TABLE etape



CREATE SEQUENCE etape_iid_etape_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE etape_iid_etape_seq


ALTER SEQUENCE etape_iid_etape_seq OWNED BY etape.iid_etape;



CREATE TABLE inclure (
    iid_etape integer NOT NULL,
    iid_typeanalyse integer NOT NULL,
    inumero integer NOT NULL,
    iecart_min integer NOT NULL,
    iecart_max integer NOT NULL
);


ALTER TABLE inclure


CREATE TABLE personnel (
    iid_personne integer NOT NULL,
    cnom character varying(50) NOT NULL,
    cprenom character varying(50) NOT NULL,
    ctel character varying(50),
    cmel character varying(50) NOT NULL,
    cmdp character varying(255) NOT NULL,
    iid_role integer NOT NULL
);


ALTER TABLE personnel



CREATE SEQUENCE personnel_iid_personne_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE personnel_iid_personne_seq



ALTER SEQUENCE personnel_iid_personne_seq OWNED BY personnel.iid_personne;


CREATE TABLE realisation (
    iid_typeanalyse integer NOT NULL,
    iid_dossier integer NOT NULL,
    iid_etape integer NOT NULL,
    iid_personne integer NOT NULL,
    tfin date NOT NULL,
    tdebut date NOT NULL
);


ALTER TABLE realisation


CREATE TABLE role (
    iid_role integer NOT NULL,
    cnom character varying(50) NOT NULL
);


ALTER TABLE role


CREATE SEQUENCE role_iid_role_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE role_iid_role_seq



ALTER SEQUENCE role_iid_role_seq OWNED BY role.iid_role;



CREATE TABLE typeanalyse (
    iid_typeanalyse integer NOT NULL,
    ccode character varying(10) NOT NULL,
    cnom character varying(50) NOT NULL
);


ALTER TABLE typeanalyse


CREATE SEQUENCE typeanalyse_iid_typeanalyse_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE typeanalyse_iid_typeanalyse_seq



ALTER SEQUENCE typeanalyse_iid_typeanalyse_seq OWNED BY typeanalyse.iid_typeanalyse;




ALTER TABLE ONLY abaque ALTER COLUMN iid_abaque SET @DEFAULT dbo.nextval(cast('abaque_iid_abaque_seq' as regclass));



ALTER TABLE ONLY client ALTER COLUMN iid_client SET @DEFAULT dbo.nextval(cast('client_iid_client_seq' as regclass));



ALTER TABLE ONLY dossier ALTER COLUMN iid_dossier SET @DEFAULT dbo.nextval(cast('dossier_iid_dossier_seq' as regclass));



ALTER TABLE ONLY etape ALTER COLUMN iid_etape SET @DEFAULT dbo.nextval(cast('etape_iid_etape_seq' as regclass));



ALTER TABLE ONLY personnel ALTER COLUMN iid_personne SET @DEFAULT dbo.nextval(cast('personnel_iid_personne_seq' as regclass));



ALTER TABLE ONLY role ALTER COLUMN iid_role SET @DEFAULT dbo.nextval(cast('role_iid_role_seq' as regclass));



ALTER TABLE ONLY typeanalyse ALTER COLUMN iid_typeanalyse SET @DEFAULT dbo.nextval(cast('typeanalyse_iid_typeanalyse_seq' as regclass));



-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (5, 5, 1, 12);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (6, 10, 1, 12);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (1, 5, 2, 1);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (2, 10, 3, 1);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (7, 15, 5, 1);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (8, 20, 6, 1);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (3, 5, 3, 2);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (4, 10, 5, 2);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (9, 15, 6, 2);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (10, 20, 7, 2);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (11, 5, 1, 3);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (12, 10, 2, 3);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (13, 15, 3, 3);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (14, 20, 4, 3);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (15, 5, 1, 4);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (16, 10, 2, 4);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (17, 15, 3, 4);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (18, 20, 4, 4);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (19, 5, 2, 5);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (20, 10, 3, 5);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (21, 15, 4, 5);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (22, 20, 5, 5);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (23, 5, 3, 6);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (24, 10, 4, 6);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (25, 15, 5, 6);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (26, 20, 5, 6);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (27, 5, 1, 7);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (28, 10, 2, 7);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (29, 15, 3, 7);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (30, 20, 4, 7);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (31, 5, 2, 8);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (32, 10, 2, 8);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (33, 15, 2, 8);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (34, 20, 2, 8);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (38, 20, 1, 9);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (37, 15, 1, 9);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (36, 10, 1, 9);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (35, 5, 1, 9);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (42, 20, 1, 10);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (41, 15, 1, 10);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (40, 10, 1, 10);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (39, 5, 1, 10);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (43, 5, 1, 11);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (44, 10, 1, 11);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (45, 15, 1, 11);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (46, 20, 1, 11);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (47, 15, 1, 12);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO abaque (iid_abaque, inb_echantillons, iduree, iid_etape) VALUES (48, 20, 1, 12);


--
-- SQLINES DEMO *** lass 0 OID 24751)
-- Dependencies: 210
-- SQLINES DEMO *** ient; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO client (iid_client, ctel, cmel, cadresse, ccp, cville, cnom) VALUES (1, '068120253238', 'sepideh@gmail.com', 'linzer straße 429 / 4010', '1140', 'Wien', 'sepideh shayesteh');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO client (iid_client, ctel, cmel, cadresse, ccp, cville, cnom) VALUES (2, '0783905558', 'Laurence@gmail.com', '17 rue du magasin,', '69700', 'Loire-sur-Rhône', 'Luarence  Eng');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO client (iid_client, ctel, cmel, cadresse, ccp, cville, cnom) VALUES (3, '0783905558', 'gustave@gmail.com', '17 rue du magasin,', '69700', 'Loire-sur-Rhône', 'Gustave Malanchane');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO client (iid_client, ctel, cmel, cadresse, ccp, cville, cnom) VALUES (4, '0783905558', 'brune@gmail.com', '17 rue du magasin,', '69700', 'Loire-sur-Rhône', 'Brune Bl');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO client (iid_client, ctel, cmel, cadresse, ccp, cville, cnom) VALUES (5, '0783905558', 'anne@gmail.com', '17 rue du magasin,', '69700', 'Loire-sur-Rhône', 'Anne lr chbr');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO client (iid_client, ctel, cmel, cadresse, ccp, cville, cnom) VALUES (6, '0783905558', 'francois', '17 rue du magasin,', '69700', 'Loire-sur-Rhône', 'Francois');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO client (iid_client, ctel, cmel, cadresse, ccp, cville, cnom) VALUES (8, '0446', 'julian@gmail.com', 'Ottobrunn,
	   Munchen', '69009', 'Lyon', 'Julian');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO client (iid_client, ctel, cmel, cadresse, ccp, cville, cnom) VALUES (9, '', '', '17 rue du magasin,', '69700', 'Loire-sur-Rhône', 'Omid amini');


--
-- SQLINES DEMO *** lass 0 OID 24866)
-- Dependencies: 225
-- SQLINES DEMO *** mpetence; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (1, 1, 5);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (1, 2, 5);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (1, 3, 5);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (1, 4, 5);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (1, 5, 5);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (1, 6, 5);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (1, 7, 5);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (1, 8, 5);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (1, 9, 5);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (1, 10, 5);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (1, 11, 5);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (1, 12, 5);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (2, 1, 4);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (2, 2, 4);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (2, 3, 4);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (2, 4, 4);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (2, 5, 4);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (2, 6, 4);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (2, 7, 4);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (2, 8, 4);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (2, 9, 4);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (2, 10, 4);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (2, 11, 4);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (2, 12, 4);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (3, 1, 3);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (3, 2, 3);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (3, 3, 3);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (3, 4, 3);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (3, 5, 3);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (3, 6, 3);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (3, 7, 3);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (3, 8, 3);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (3, 9, 3);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (3, 10, 3);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (3, 11, 3);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (3, 12, 3);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (4, 1, 2);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (4, 2, 2);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (4, 3, 2);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (4, 4, 2);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (4, 5, 2);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (4, 6, 2);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (4, 7, 2);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (4, 8, 2);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (4, 9, 2);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (4, 10, 2);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (4, 11, 2);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO competence (iid_personne, iid_etape, iniveau) VALUES (4, 12, 2);


--
-- SQLINES DEMO *** lass 0 OID 24836)
-- Dependencies: 223
-- SQLINES DEMO *** ntenir; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO contenir (iid_typeanalyse, iid_dossier) VALUES (1, 1);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO contenir (iid_typeanalyse, iid_dossier) VALUES (2, 1);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO contenir (iid_typeanalyse, iid_dossier) VALUES (1, 2);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO contenir (iid_typeanalyse, iid_dossier) VALUES (3, 3);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO contenir (iid_typeanalyse, iid_dossier) VALUES (1, 4);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO contenir (iid_typeanalyse, iid_dossier) VALUES (2, 5);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO contenir (iid_typeanalyse, iid_dossier) VALUES (1, 6);


--
-- SQLINES DEMO *** lass 0 OID 24801)
-- Dependencies: 220
-- SQLINES DEMO *** ssier; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO dossier (iid_dossier, darrive_theo, darrive_reel, inombre_echantillon, iordre_priorite, cnum, cnom, iid_client, iid_personne) VALUES (2, '2022-09-20', '2022-09-21', 10, 1, '209', 'val de loire', 3, 3);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO dossier (iid_dossier, darrive_theo, darrive_reel, inombre_echantillon, iordre_priorite, cnum, cnom, iid_client, iid_personne) VALUES (3, '2022-09-25', '2022-09-26', 20, 2, '210', 'Brune bl', 4, 2);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO dossier (iid_dossier, darrive_theo, darrive_reel, inombre_echantillon, iordre_priorite, cnum, cnom, iid_client, iid_personne) VALUES (5, '2022-09-30', '2022-09-30', 5, 3, '212', 'Francois', 6, 4);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO dossier (iid_dossier, darrive_theo, darrive_reel, inombre_echantillon, iordre_priorite, cnum, cnom, iid_client, iid_personne) VALUES (4, '2022-09-28', '2022-09-28', 20, 1, '211', 'Anne lr', 5, 3);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO dossier (iid_dossier, darrive_theo, darrive_reel, inombre_echantillon, iordre_priorite, cnum, cnom, iid_client, iid_personne) VALUES (1, '2022-09-01', '2022-09-01', 5, 1, '208', 'perche', 2, 2);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO dossier (iid_dossier, darrive_theo, darrive_reel, inombre_echantillon, iordre_priorite, cnum, cnom, iid_client, iid_personne) VALUES (6, '2022-12-13', '2022-12-16', 10, 2, '400', 'dossierAMINI', 9, 3);


--
-- SQLINES DEMO *** lass 0 OID 24769)
-- Dependencies: 214
-- SQLINES DEMO *** ape; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO etape (iid_etape, cdescription, cnom) VALUES (1, 'vérification, mise on en ordre, mise au frigo', 'Réception');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO etape (iid_etape, cdescription, cnom) VALUES (2, 'Pesée, enregistrement', 'Préparation');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO etape (iid_etape, cdescription, cnom) VALUES (3, 'Préparation tamis, Coulage, sur tamis + vaisselle, récupération en Falcon + vaisselle', 'Extraction');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO etape (iid_etape, cdescription, cnom) VALUES (4, 'Comptage + Identification des phytoparasites, réduction en tube hémolyse + vaisselle', 'Comptage');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO etape (iid_etape, cdescription, cnom) VALUES (5, 'Fixation (réduction volume + ajout FPG chaud)', 'Fixation');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO etape (iid_etape, cdescription, cnom) VALUES (6, 'Stérilisation sols à l&#39;étuve (remplir et vider)', 'Stérilisation');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO etape (iid_etape, cdescription, cnom) VALUES (7, 'Rejet sol à la déchetterie', 'Rejet sol');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO etape (iid_etape, cdescription, cnom) VALUES (8, 'Mise à sécher sol', 'Séchage');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO etape (iid_etape, cdescription, cnom) VALUES (9, 'Tamisage pour physico-chime', 'Tamisage');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO etape (iid_etape, cdescription, cnom) VALUES (10, 'Informatisation-fichier pour importa dans l&#39;application Elipto, Import fichier Elipto, correction', 'Elipto');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO etape (iid_etape, cdescription, cnom) VALUES (11, 'Création de fiche avec l&#39;application Edito, diagnostic, correction fiche Edito', 'Edito');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO etape (iid_etape, cdescription, cnom) VALUES (12, 'Rédaction du mail et envoi au client', 'Envoi des résultts');


--
-- SQLINES DEMO *** lass 0 OID 24851)
-- Dependencies: 224
-- SQLINES DEMO *** clure; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO inclure (iid_etape, iid_typeanalyse, inumero, iecart_min, iecart_max) VALUES (1, 1, 1, 1, 2);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO inclure (iid_etape, iid_typeanalyse, inumero, iecart_min, iecart_max) VALUES (2, 1, 3, 1, 5);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO inclure (iid_etape, iid_typeanalyse, inumero, iecart_min, iecart_max) VALUES (1, 2, 1, 1, 2);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO inclure (iid_etape, iid_typeanalyse, inumero, iecart_min, iecart_max) VALUES (3, 1, 3, 1, 6);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO inclure (iid_etape, iid_typeanalyse, inumero, iecart_min, iecart_max) VALUES (4, 1, 4, 1, 5);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO inclure (iid_etape, iid_typeanalyse, inumero, iecart_min, iecart_max) VALUES (5, 1, 5, 1, 5);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO inclure (iid_etape, iid_typeanalyse, inumero, iecart_min, iecart_max) VALUES (6, 1, 6, 3, 6);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO inclure (iid_etape, iid_typeanalyse, inumero, iecart_min, iecart_max) VALUES (7, 1, 7, 1, 2);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO inclure (iid_etape, iid_typeanalyse, inumero, iecart_min, iecart_max) VALUES (8, 1, 8, 2, 4);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO inclure (iid_etape, iid_typeanalyse, inumero, iecart_min, iecart_max) VALUES (9, 1, 10, 2, 5);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO inclure (iid_etape, iid_typeanalyse, inumero, iecart_min, iecart_max) VALUES (10, 1, 11, 1, 2);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO inclure (iid_etape, iid_typeanalyse, inumero, iecart_min, iecart_max) VALUES (11, 1, 13, 1, 2);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO inclure (iid_etape, iid_typeanalyse, inumero, iecart_min, iecart_max) VALUES (12, 1, 14, 1, 5);


--
-- SQLINES DEMO *** lass 0 OID 24789)
-- Dependencies: 218
-- SQLINES DEMO *** rsonnel; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO personnel (iid_personne, cnom, cprenom, ctel, cmel, cmdp, iid_role) VALUES (1, 'Amini', 'Omid', '0783905558', 'bcs.omid@gmail.com', '$2y$10$ugckGtM1M.CFw6FNwz3DVOrVc2aecjOw4.e9nV26XqL0JUpk6.GeW', 1);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO personnel (iid_personne, cnom, cprenom, ctel, cmel, cmdp, iid_role) VALUES (2, 'Cote', 'Xavier', '0783905558', 'xaviercote@gmail.com', '$2y$10$qBBreXLTez5bK6GAPCdP/.d3IvQHigzyIXtkFwxzyC2fTnhhXHQ/q', 2);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO personnel (iid_personne, cnom, cprenom, ctel, cmel, cmdp, iid_role) VALUES (3, 'Bouvard', 'Valentin', '0783905558', 'valentinbouvard@gmail.com', '$2y$10$rNZX4r30BnbYJNGP7XrYS.9pzRqefSpMuVLxS8lNDA6wgAxC1x73u', 2);
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO personnel (iid_personne, cnom, cprenom, ctel, cmel, cmdp, iid_role) VALUES (4, 'Robert', 'Loic', '0783905558', 'loicrobert@gmail.com', '$2y$10$JXA1Jlc4WZcHffnRX9BF8u0EQeJpNTG5yXrH.Uj.BRvkPhPgI9Jyy', 2);


--
-- SQLINES DEMO *** lass 0 OID 24881)
-- Dependencies: 226
-- SQLINES DEMO *** alisation; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO realisation (iid_typeanalyse, iid_dossier, iid_etape, iid_personne, tfin, tdebut) VALUES (2, 5, 1, 4, '2022-09-30', '2022-09-30');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO realisation (iid_typeanalyse, iid_dossier, iid_etape, iid_personne, tfin, tdebut) VALUES (1, 4, 1, 4, '2022-09-28', '2022-09-28');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO realisation (iid_typeanalyse, iid_dossier, iid_etape, iid_personne, tfin, tdebut) VALUES (1, 4, 2, 4, '2022-09-28', '2022-09-28');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO realisation (iid_typeanalyse, iid_dossier, iid_etape, iid_personne, tfin, tdebut) VALUES (1, 4, 3, 4, '2022-09-28', '2022-09-28');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO realisation (iid_typeanalyse, iid_dossier, iid_etape, iid_personne, tfin, tdebut) VALUES (1, 4, 4, 1, '2022-10-03', '2022-10-03');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO realisation (iid_typeanalyse, iid_dossier, iid_etape, iid_personne, tfin, tdebut) VALUES (1, 4, 5, 2, '2022-10-05', '2022-10-05');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO realisation (iid_typeanalyse, iid_dossier, iid_etape, iid_personne, tfin, tdebut) VALUES (2, 1, 1, 2, '2022-09-01', '2022-09-01');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO realisation (iid_typeanalyse, iid_dossier, iid_etape, iid_personne, tfin, tdebut) VALUES (1, 1, 1, 4, '2022-09-01', '2022-09-01');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO realisation (iid_typeanalyse, iid_dossier, iid_etape, iid_personne, tfin, tdebut) VALUES (1, 1, 2, 2, '2022-09-02', '2022-09-02');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO realisation (iid_typeanalyse, iid_dossier, iid_etape, iid_personne, tfin, tdebut) VALUES (1, 1, 3, 2, '2022-09-03', '2022-09-03');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO realisation (iid_typeanalyse, iid_dossier, iid_etape, iid_personne, tfin, tdebut) VALUES (1, 1, 4, 3, '2022-09-03', '2022-09-03');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO realisation (iid_typeanalyse, iid_dossier, iid_etape, iid_personne, tfin, tdebut) VALUES (1, 1, 5, 2, '2022-09-05', '2022-09-05');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO realisation (iid_typeanalyse, iid_dossier, iid_etape, iid_personne, tfin, tdebut) VALUES (1, 1, 6, 2, '2022-09-02', '2022-09-02');


--
-- SQLINES DEMO *** lass 0 OID 24780)
-- Dependencies: 216
-- SQLINES DEMO *** le; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO role (iid_role, cnom) VALUES (1, 'Administrateur');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO role (iid_role, cnom) VALUES (2, 'technicien');


--
-- SQLINES DEMO *** lass 0 OID 24760)
-- Dependencies: 212
-- SQLINES DEMO *** peanalyse; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO typeanalyse (iid_typeanalyse, ccode, cnom) VALUES (1, '24', 'bioA');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO typeanalyse (iid_typeanalyse, ccode, cnom) VALUES (2, '20', 'phyS');
-- SQLINES LICENSE FOR EVALUATION USE ONLY
INSERT INTO typeanalyse (iid_typeanalyse, ccode, cnom) VALUES (3, '14', 'kyQ+');


--
-- SQLINES DEMO *** lass 0 OID 0)
-- Dependencies: 221
-- SQLINES DEMO *** abaque_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

-- SQLINES LICENSE FOR EVALUATION USE ONLY
SELECT pg_catalog.setval('abaque_iid_abaque_seq', 48, true);


--
-- SQLINES DEMO *** lass 0 OID 0)
-- Dependencies: 209
-- SQLINES DEMO *** client_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

-- SQLINES LICENSE FOR EVALUATION USE ONLY
SELECT pg_catalog.setval('client_iid_client_seq', 9, true);


--
-- SQLINES DEMO *** lass 0 OID 0)
-- Dependencies: 219
-- SQLINES DEMO *** _dossier_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

-- SQLINES LICENSE FOR EVALUATION USE ONLY
SELECT pg_catalog.setval('dossier_iid_dossier_seq', 6, true);


--
-- SQLINES DEMO *** lass 0 OID 0)
-- Dependencies: 213
-- SQLINES DEMO *** tape_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

-- SQLINES LICENSE FOR EVALUATION USE ONLY
SELECT pg_catalog.setval('etape_iid_etape_seq', 12, true);


--
-- SQLINES DEMO *** lass 0 OID 0)
-- Dependencies: 217
-- SQLINES DEMO *** id_personne_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

-- SQLINES LICENSE FOR EVALUATION USE ONLY
SELECT pg_catalog.setval('personnel_iid_personne_seq', 4, true);


--
-- SQLINES DEMO *** lass 0 OID 0)
-- Dependencies: 215
-- SQLINES DEMO *** le_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

-- SQLINES LICENSE FOR EVALUATION USE ONLY
SELECT pg_catalog.setval('role_iid_role_seq', 1, false);


--
-- SQLINES DEMO *** lass 0 OID 0)
-- Dependencies: 211
-- SQLINES DEMO *** _iid_typeanalyse_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

-- SQLINES LICENSE FOR EVALUATION USE ONLY
SELECT pg_catalog.setval('typeanalyse_iid_typeanalyse_seq', 3, true);


--
-- SQLINES DEMO *** lass 2606 OID 24830)
-- SQLINES DEMO *** ue_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY abaque
    ADD CONSTRAINT abaque_pk PRIMARY dbo.KEY (iid_abaque);


--
-- SQLINES DEMO *** lass 2606 OID 24758)
-- SQLINES DEMO *** nt_ak; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY client
    ADD CONSTRAINT client_ak dbo.UNIQUE (cnom);


--
-- SQLINES DEMO *** lass 2606 OID 24756)
-- SQLINES DEMO *** nt_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY client
    ADD CONSTRAINT client_pk PRIMARY dbo.KEY (iid_client);


--
-- SQLINES DEMO *** lass 2606 OID 24870)
-- SQLINES DEMO *** competence_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY competence
    ADD CONSTRAINT competence_pk PRIMARY dbo.KEY (iid_personne, iid_etape);


--
-- SQLINES DEMO *** lass 2606 OID 24840)
-- SQLINES DEMO *** ntenir_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY contenir
    ADD CONSTRAINT contenir_pk PRIMARY dbo.KEY (iid_typeanalyse, iid_dossier);


--
-- SQLINES DEMO *** lass 2606 OID 24808)
-- SQLINES DEMO *** sier_ak; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dossier
    ADD CONSTRAINT dossier_ak dbo.UNIQUE (cnum, cnom);


--
-- SQLINES DEMO *** lass 2606 OID 24806)
-- SQLINES DEMO *** sier_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dossier
    ADD CONSTRAINT dossier_pk PRIMARY dbo.KEY (iid_dossier);


--
-- SQLINES DEMO *** lass 2606 OID 24778)
-- SQLINES DEMO *** _ak; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY etape
    ADD CONSTRAINT etape_ak dbo.UNIQUE (cnom);


--
-- SQLINES DEMO *** lass 2606 OID 24776)
-- SQLINES DEMO *** _pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY etape
    ADD CONSTRAINT etape_pk PRIMARY dbo.KEY (iid_etape);


--
-- SQLINES DEMO *** lass 2606 OID 24855)
-- SQLINES DEMO *** lure_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inclure
    ADD CONSTRAINT inclure_pk PRIMARY dbo.KEY (iid_etape, iid_typeanalyse);


--
-- SQLINES DEMO *** lass 2606 OID 24794)
-- SQLINES DEMO *** ersonnel_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY personnel
    ADD CONSTRAINT personnel_pk PRIMARY dbo.KEY (iid_personne);


--
-- SQLINES DEMO *** lass 2606 OID 24787)
-- SQLINES DEMO *** k; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_ak dbo.UNIQUE (cnom);


--
-- SQLINES DEMO *** lass 2606 OID 24785)
-- SQLINES DEMO *** k; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_pk PRIMARY dbo.KEY (iid_role);


--
-- SQLINES DEMO *** lass 2606 OID 24767)
-- SQLINES DEMO ***  typeanalyse_ak; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY typeanalyse
    ADD CONSTRAINT typeanalyse_ak dbo.UNIQUE (ccode, cnom);


--
-- SQLINES DEMO *** lass 2606 OID 24765)
-- SQLINES DEMO ***  typeanalyse_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY typeanalyse
    ADD CONSTRAINT typeanalyse_pk PRIMARY dbo.KEY (iid_typeanalyse);


--
-- SQLINES DEMO *** lass 2606 OID 24831)
-- SQLINES DEMO *** ue_etape_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY abaque
    ADD CONSTRAINT abaque_etape_fk FOREIGN dbo.KEY (iid_etape) REFERENCES etape(iid_etape);


--
-- SQLINES DEMO *** lass 2606 OID 24876)
-- SQLINES DEMO *** competence_etape0_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY competence
    ADD CONSTRAINT competence_etape0_fk FOREIGN dbo.KEY (iid_etape) REFERENCES etape(iid_etape);


--
-- SQLINES DEMO *** lass 2606 OID 24871)
-- SQLINES DEMO *** competence_personnel_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY competence
    ADD CONSTRAINT competence_personnel_fk FOREIGN dbo.KEY (iid_personne) REFERENCES personnel(iid_personne);


--
-- SQLINES DEMO *** lass 2606 OID 24846)
-- SQLINES DEMO *** ntenir_dossier0_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY contenir
    ADD CONSTRAINT contenir_dossier0_fk FOREIGN dbo.KEY (iid_dossier) REFERENCES dossier(iid_dossier);


--
-- SQLINES DEMO *** lass 2606 OID 24841)
-- SQLINES DEMO *** ntenir_typeanalyse_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY contenir
    ADD CONSTRAINT contenir_typeanalyse_fk FOREIGN dbo.KEY (iid_typeanalyse) REFERENCES typeanalyse(iid_typeanalyse);


--
-- SQLINES DEMO *** lass 2606 OID 24809)
-- SQLINES DEMO *** sier_client_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dossier
    ADD CONSTRAINT dossier_client_fk FOREIGN dbo.KEY (iid_client) REFERENCES client(iid_client);


--
-- SQLINES DEMO *** lass 2606 OID 24814)
-- SQLINES DEMO *** sier_personnel0_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dossier
    ADD CONSTRAINT dossier_personnel0_fk FOREIGN dbo.KEY (iid_personne) REFERENCES personnel(iid_personne);


--
-- SQLINES DEMO *** lass 2606 OID 24856)
-- SQLINES DEMO *** lure_etape_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inclure
    ADD CONSTRAINT inclure_etape_fk FOREIGN dbo.KEY (iid_etape) REFERENCES etape(iid_etape);


--
-- SQLINES DEMO *** lass 2606 OID 24861)
-- SQLINES DEMO *** lure_typeanalyse0_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inclure
    ADD CONSTRAINT inclure_typeanalyse0_fk FOREIGN dbo.KEY (iid_typeanalyse) REFERENCES typeanalyse(iid_typeanalyse);


--
-- SQLINES DEMO *** lass 2606 OID 24795)
-- SQLINES DEMO *** ersonnel_role_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY personnel
    ADD CONSTRAINT personnel_role_fk FOREIGN dbo.KEY (iid_role) REFERENCES role(iid_role);


--
-- SQLINES DEMO *** lass 2606 OID 24891)
-- SQLINES DEMO ***  realisation_dossier0_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY realisation
    ADD CONSTRAINT realisation_dossier0_fk FOREIGN dbo.KEY (iid_dossier) REFERENCES dossier(iid_dossier);


--
-- SQLINES DEMO *** lass 2606 OID 24901)
-- SQLINES DEMO ***  realisation_etape2_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY realisation
    ADD CONSTRAINT realisation_etape2_fk FOREIGN dbo.KEY (iid_etape) REFERENCES etape(iid_etape);


--
-- SQLINES DEMO *** lass 2606 OID 24906)
-- SQLINES DEMO ***  realisation_personnel3_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY realisation
    ADD CONSTRAINT realisation_personnel3_fk FOREIGN dbo.KEY (iid_personne) REFERENCES personnel(iid_personne);


--
-- SQLINES DEMO *** lass 2606 OID 24886)
-- SQLINES DEMO ***  realisation_typeanalyse_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY realisation
    ADD CONSTRAINT realisation_typeanalyse_fk FOREIGN dbo.KEY (iid_typeanalyse) REFERENCES typeanalyse(iid_typeanalyse);


-- SQLINES DEMO *** -12-13 12:32:15

--
-- SQLINES DEMO *** se dump complete
--

