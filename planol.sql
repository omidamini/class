/*------------------------------------------------------------
*        Script SQLSERVER 
------------------------------------------------------------*/


/*------------------------------------------------------------
-- Table: Client
------------------------------------------------------------*/
CREATE TABLE Client(
	iid_client         INT IDENTITY (1,1) NOT NULL ,
	ctel_client        VARCHAR (15)  ,
	cmel_client        VARCHAR (50)  ,
	cAddresse_client   VARCHAR (50)  ,
	ccp_client         VARCHAR (10)  ,
	cville_client      VARCHAR (50)  ,
	cnom_client        VARCHAR (50) NOT NULL  ,
	CONSTRAINT Client_PK PRIMARY KEY (iid_client) ,
	CONSTRAINT Client_AK UNIQUE (cnom_client)
);


/*------------------------------------------------------------
-- Table: TypeAnalyse
------------------------------------------------------------*/
CREATE TABLE TypeAnalyse(
	iid_type_analyse     INT IDENTITY (1,1) NOT NULL ,
	ccode_type_analyse   VARCHAR (10) NOT NULL ,
	cnom_type_analyse    VARCHAR (50) NOT NULL  ,
	CONSTRAINT TypeAnalyse_PK PRIMARY KEY (iid_type_analyse) ,
	CONSTRAINT TypeAnalyse_AK UNIQUE (ccode_type_analyse,cnom_type_analyse)
);


/*------------------------------------------------------------
-- Table: Operation
------------------------------------------------------------*/
CREATE TABLE Operation(
	iid_operation   INT IDENTITY (1,1) NOT NULL ,
	cdescription    TEXT   ,
	cnom_etape      VARCHAR (50) NOT NULL  ,
	CONSTRAINT Operation_PK PRIMARY KEY (iid_operation) ,
	CONSTRAINT Operation_AK UNIQUE (cnom_etape)
);


/*------------------------------------------------------------
-- Table: Role
------------------------------------------------------------*/
CREATE TABLE Role(
	iid_role    INT IDENTITY (1,1) NOT NULL ,
	cnom_role   VARCHAR (50) NOT NULL  ,
	CONSTRAINT Role_PK PRIMARY KEY (iid_role) ,
	CONSTRAINT Role_AK UNIQUE (cnom_role)
);


/*------------------------------------------------------------
-- Table: Personnel
------------------------------------------------------------*/
CREATE TABLE Personnel(
	iid_personne       INT IDENTITY (1,1) NOT NULL ,
	cnom_personne      VARCHAR (50) NOT NULL ,
	cprenom_personne   VARCHAR (50) NOT NULL ,
	ctel_personne      VARCHAR (50)  ,
	cmel_personne      VARCHAR (50) NOT NULL ,
	iid_role           INT  NOT NULL  ,
	CONSTRAINT Personnel_PK PRIMARY KEY (iid_personne)

	,CONSTRAINT Personnel_Role_FK FOREIGN KEY (iid_role) REFERENCES Role(iid_role)
);


/*------------------------------------------------------------
-- Table: Dossier
------------------------------------------------------------*/
CREATE TABLE Dossier(
	iid_dossier           INT IDENTITY (1,1) NOT NULL ,
	darrive_theo          DATETIME NOT NULL ,
	darrive_reel          DATETIME  ,
	inombre_echantillon   INT  NOT NULL ,
	iordre_priorite       INT  NOT NULL ,
	cnum_dossier          VARCHAR (10) NOT NULL ,
	cnom_dossier          VARCHAR (50) NOT NULL ,
	iid_client            INT  NOT NULL ,
	iid_personne          INT  NOT NULL  ,
	CONSTRAINT Dossier_PK PRIMARY KEY (iid_dossier) ,
	CONSTRAINT Dossier_AK UNIQUE (cnum_dossier,cnom_dossier)

	,CONSTRAINT Dossier_Client_FK FOREIGN KEY (iid_client) REFERENCES Client(iid_client)
	,CONSTRAINT Dossier_Personnel0_FK FOREIGN KEY (iid_personne) REFERENCES Personnel(iid_personne)
);


/*------------------------------------------------------------
-- Table: Date
------------------------------------------------------------*/
CREATE TABLE Date(
	tHeure_Debut   DATETIME NOT NULL  ,
	CONSTRAINT Date_PK PRIMARY KEY (tHeure_Debut)
);


/*------------------------------------------------------------
-- Table: Abaque
------------------------------------------------------------*/
CREATE TABLE Abaque(
	iid_abaque         INT IDENTITY (1,1) NOT NULL ,
	inb_echantillons   INT  NOT NULL ,
	iduree             INT  NOT NULL ,
	iid_operation      INT  NOT NULL  ,
	CONSTRAINT Abaque_PK PRIMARY KEY (iid_abaque)

	,CONSTRAINT Abaque_Operation_FK FOREIGN KEY (iid_operation) REFERENCES Operation(iid_operation)
);


/*------------------------------------------------------------
-- Table: Contenir
------------------------------------------------------------*/
CREATE TABLE Contenir(
	iid_type_analyse   INT  NOT NULL ,
	iid_dossier        INT  NOT NULL  ,
	CONSTRAINT Contenir_PK PRIMARY KEY (iid_type_analyse,iid_dossier)

	,CONSTRAINT Contenir_TypeAnalyse_FK FOREIGN KEY (iid_type_analyse) REFERENCES TypeAnalyse(iid_type_analyse)
	,CONSTRAINT Contenir_Dossier0_FK FOREIGN KEY (iid_dossier) REFERENCES Dossier(iid_dossier)
);


/*------------------------------------------------------------
-- Table: Inclure
------------------------------------------------------------*/
CREATE TABLE Inclure(
	iid_operation      INT  NOT NULL ,
	iid_type_analyse   INT  NOT NULL ,
	inumero            INT  NOT NULL ,
	iecart_min         INT  NOT NULL ,
	iecart_max         INT  NOT NULL  ,
	CONSTRAINT Inclure_PK PRIMARY KEY (iid_operation,iid_type_analyse)

	,CONSTRAINT Inclure_Operation_FK FOREIGN KEY (iid_operation) REFERENCES Operation(iid_operation)
	,CONSTRAINT Inclure_TypeAnalyse0_FK FOREIGN KEY (iid_type_analyse) REFERENCES TypeAnalyse(iid_type_analyse)
);


/*------------------------------------------------------------
-- Table: Avoir (compétence)
------------------------------------------------------------*/
CREATE TABLE Avoir__competence_(
	iid_personne    INT  NOT NULL ,
	iid_operation   INT  NOT NULL ,
	iniveau         INT  NOT NULL  ,
	CONSTRAINT Avoir__competence__PK PRIMARY KEY (iid_personne,iid_operation)

	,CONSTRAINT Avoir__competence__Personnel_FK FOREIGN KEY (iid_personne) REFERENCES Personnel(iid_personne)
	,CONSTRAINT Avoir__competence__Operation0_FK FOREIGN KEY (iid_operation) REFERENCES Operation(iid_operation)
);


/*------------------------------------------------------------
-- Table: Realiser
------------------------------------------------------------*/
CREATE TABLE Realiser(
	iid_type_analyse   INT  NOT NULL ,
	iid_dossier        INT  NOT NULL ,
	tHeure_Debut       DATETIME NOT NULL ,
	iid_operation      INT  NOT NULL ,
	iid_personne       INT  NOT NULL  ,
	CONSTRAINT Realiser_PK PRIMARY KEY (iid_type_analyse,iid_dossier,tHeure_Debut,iid_operation,iid_personne)

	,CONSTRAINT Realiser_TypeAnalyse_FK FOREIGN KEY (iid_type_analyse) REFERENCES TypeAnalyse(iid_type_analyse)
	,CONSTRAINT Realiser_Dossier0_FK FOREIGN KEY (iid_dossier) REFERENCES Dossier(iid_dossier)
	,CONSTRAINT Realiser_Date1_FK FOREIGN KEY (tHeure_Debut) REFERENCES Date(tHeure_Debut)
	,CONSTRAINT Realiser_Operation2_FK FOREIGN KEY (iid_operation) REFERENCES Operation(iid_operation)
	,CONSTRAINT Realiser_Personnel3_FK FOREIGN KEY (iid_personne) REFERENCES Personnel(iid_personne)
);



