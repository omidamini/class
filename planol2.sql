--
-- PostgreSQL database dump
--

-- Dumped from database version 14.3
-- Dumped by pg_dump version 14.3

-- Started on 2022-12-13 11:56:35

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3399 (class 0 OID 24769)
-- Dependencies: 214
-- Data for Name: etape; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.etape (iid_etape, cdescription, cnom) FROM stdin;
1	vérification, mise on en ordre, mise au frigo	Réception
2	Pesée, enregistrement	Préparation
3	Préparation tamis, Coulage, sur tamis + vaisselle, récupération en Falcon + vaisselle	Extraction
4	Comptage + Identification des phytoparasites, réduction en tube hémolyse + vaisselle	Comptage
5	Fixation (réduction volume + ajout FPG chaud)	Fixation
6	Stérilisation sols à l&#39;étuve (remplir et vider)	Stérilisation
7	Rejet sol à la déchetterie	Rejet sol
8	Mise à sécher sol	Séchage
9	Tamisage pour physico-chime	Tamisage
10	Informatisation-fichier pour importa dans l&#39;application Elipto, Import fichier Elipto, correction	Elipto
11	Création de fiche avec l&#39;application Edito, diagnostic, correction fiche Edito	Edito
12	Rédaction du mail et envoi au client	Envoi des résultts
\.


--
-- TOC entry 3407 (class 0 OID 24825)
-- Dependencies: 222
-- Data for Name: abaque; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.abaque (iid_abaque, inb_echantillons, iduree, iid_etape) FROM stdin;
5	5	1	12
6	10	1	12
1	5	2	1
2	10	3	1
7	15	5	1
8	20	6	1
3	5	3	2
4	10	5	2
9	15	6	2
10	20	7	2
11	5	1	3
12	10	2	3
13	15	3	3
14	20	4	3
15	5	1	4
16	10	2	4
17	15	3	4
18	20	4	4
19	5	2	5
20	10	3	5
21	15	4	5
22	20	5	5
23	5	3	6
24	10	4	6
25	15	5	6
26	20	5	6
27	5	1	7
28	10	2	7
29	15	3	7
30	20	4	7
31	5	2	8
32	10	2	8
33	15	2	8
34	20	2	8
38	20	1	9
37	15	1	9
36	10	1	9
35	5	1	9
42	20	1	10
41	15	1	10
40	10	1	10
39	5	1	10
43	5	1	11
44	10	1	11
45	15	1	11
46	20	1	11
47	15	1	12
48	20	1	12
\.


--
-- TOC entry 3395 (class 0 OID 24751)
-- Dependencies: 210
-- Data for Name: client; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client (iid_client, ctel, cmel, cadresse, ccp, cville, cnom) FROM stdin;
1	068120253238	sepideh@gmail.com	linzer straße 429 / 4010	1140	Wien	sepideh shayesteh
2	0783905558	Laurence@gmail.com	17 rue du magasin,	69700	Loire-sur-Rhône	Luarence  Eng
3	0783905558	gustave@gmail.com	17 rue du magasin,	69700	Loire-sur-Rhône	Gustave Malanchane
4	0783905558	brune@gmail.com	17 rue du magasin,	69700	Loire-sur-Rhône	Brune Bl
5	0783905558	anne@gmail.com	17 rue du magasin,	69700	Loire-sur-Rhône	Anne lr chbr
6	0783905558	francois	17 rue du magasin,	69700	Loire-sur-Rhône	Francois
8	0446	julian@gmail.com	Ottobrunn,\n\t   Munchen	69009	Lyon	Julian
9			17 rue du magasin,	69700	Loire-sur-Rhône	Omid amini
\.


--
-- TOC entry 3401 (class 0 OID 24780)
-- Dependencies: 216
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.role (iid_role, cnom) FROM stdin;
1	Administrateur
2	technicien
\.


--
-- TOC entry 3403 (class 0 OID 24789)
-- Dependencies: 218
-- Data for Name: personnel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.personnel (iid_personne, cnom, cprenom, ctel, cmel, cmdp, iid_role) FROM stdin;
1	Amini	Omid	0783905558	bcs.omid@gmail.com	$2y$10$ugckGtM1M.CFw6FNwz3DVOrVc2aecjOw4.e9nV26XqL0JUpk6.GeW	1
2	Cote	Xavier	0783905558	xaviercote@gmail.com	$2y$10$qBBreXLTez5bK6GAPCdP/.d3IvQHigzyIXtkFwxzyC2fTnhhXHQ/q	2
3	Bouvard	Valentin	0783905558	valentinbouvard@gmail.com	$2y$10$rNZX4r30BnbYJNGP7XrYS.9pzRqefSpMuVLxS8lNDA6wgAxC1x73u	2
4	Robert	Loic	0783905558	loicrobert@gmail.com	$2y$10$JXA1Jlc4WZcHffnRX9BF8u0EQeJpNTG5yXrH.Uj.BRvkPhPgI9Jyy	2
\.


--
-- TOC entry 3410 (class 0 OID 24866)
-- Dependencies: 225
-- Data for Name: competence; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.competence (iid_personne, iid_etape, iniveau) FROM stdin;
1	1	5
1	2	5
1	3	5
1	4	5
1	5	5
1	6	5
1	7	5
1	8	5
1	9	5
1	10	5
1	11	5
1	12	5
2	1	4
2	2	4
2	3	4
2	4	4
2	5	4
2	6	4
2	7	4
2	8	4
2	9	4
2	10	4
2	11	4
2	12	4
3	1	3
3	2	3
3	3	3
3	4	3
3	5	3
3	6	3
3	7	3
3	8	3
3	9	3
3	10	3
3	11	3
3	12	3
4	1	2
4	2	2
4	3	2
4	4	2
4	5	2
4	6	2
4	7	2
4	8	2
4	9	2
4	10	2
4	11	2
4	12	2
\.


--
-- TOC entry 3405 (class 0 OID 24801)
-- Dependencies: 220
-- Data for Name: dossier; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.dossier (iid_dossier, darrive_theo, darrive_reel, inombre_echantillon, iordre_priorite, cnum, cnom, iid_client, iid_personne) FROM stdin;
2	2022-09-20	2022-09-21	10	1	209	val de loire	3	3
3	2022-09-25	2022-09-26	20	2	210	Brune bl	4	2
5	2022-09-30	2022-09-30	5	3	212	Francois	6	4
4	2022-09-28	2022-09-28	20	1	211	Anne lr	5	3
1	2022-09-01	2022-09-01	5	1	208	perche	2	2
6	2022-12-13	2022-12-16	10	2	400	dossierAMINI	9	3
\.


--
-- TOC entry 3397 (class 0 OID 24760)
-- Dependencies: 212
-- Data for Name: typeanalyse; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.typeanalyse (iid_typeanalyse, ccode, cnom) FROM stdin;
1	24	bioA
2	20	phyS
3	14	kyQ+
\.


--
-- TOC entry 3408 (class 0 OID 24836)
-- Dependencies: 223
-- Data for Name: contenir; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.contenir (iid_typeanalyse, iid_dossier) FROM stdin;
1	1
2	1
1	2
3	3
1	4
2	5
1	6
\.


--
-- TOC entry 3409 (class 0 OID 24851)
-- Dependencies: 224
-- Data for Name: inclure; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.inclure (iid_etape, iid_typeanalyse, inumero, iecart_min, iecart_max) FROM stdin;
1	1	1	1	2
2	1	3	1	5
1	2	1	1	2
3	1	3	1	6
4	1	4	1	5
5	1	5	1	5
6	1	6	3	6
7	1	7	1	2
8	1	8	2	4
9	1	10	2	5
10	1	11	1	2
11	1	13	1	2
12	1	14	1	5
\.


--
-- TOC entry 3411 (class 0 OID 24881)
-- Dependencies: 226
-- Data for Name: realisation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.realisation (iid_typeanalyse, iid_dossier, iid_etape, iid_personne, tfin, tdebut) FROM stdin;
2	5	1	4	2022-09-30	2022-09-30
1	4	1	4	2022-09-28	2022-09-28
1	4	2	4	2022-09-28	2022-09-28
1	4	3	4	2022-09-28	2022-09-28
1	4	4	1	2022-10-03	2022-10-03
1	4	5	2	2022-10-05	2022-10-05
2	1	1	2	2022-09-01	2022-09-01
1	1	1	4	2022-09-01	2022-09-01
1	1	2	2	2022-09-02	2022-09-02
1	1	3	2	2022-09-03	2022-09-03
1	1	4	3	2022-09-03	2022-09-03
1	1	5	2	2022-09-05	2022-09-05
1	1	6	2	2022-09-02	2022-09-02
\.


--
-- TOC entry 3417 (class 0 OID 0)
-- Dependencies: 221
-- Name: abaque_iid_abaque_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.abaque_iid_abaque_seq', 48, true);


--
-- TOC entry 3418 (class 0 OID 0)
-- Dependencies: 209
-- Name: client_iid_client_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.client_iid_client_seq', 9, true);


--
-- TOC entry 3419 (class 0 OID 0)
-- Dependencies: 219
-- Name: dossier_iid_dossier_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.dossier_iid_dossier_seq', 6, true);


--
-- TOC entry 3420 (class 0 OID 0)
-- Dependencies: 213
-- Name: etape_iid_etape_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.etape_iid_etape_seq', 12, true);


--
-- TOC entry 3421 (class 0 OID 0)
-- Dependencies: 217
-- Name: personnel_iid_personne_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.personnel_iid_personne_seq', 4, true);


--
-- TOC entry 3422 (class 0 OID 0)
-- Dependencies: 215
-- Name: role_iid_role_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.role_iid_role_seq', 1, false);


--
-- TOC entry 3423 (class 0 OID 0)
-- Dependencies: 211
-- Name: typeanalyse_iid_typeanalyse_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.typeanalyse_iid_typeanalyse_seq', 3, true);


-- Completed on 2022-12-13 11:56:35

--
-- PostgreSQL database dump complete
--

