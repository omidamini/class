

DROP TABLE IF EXISTS article;

CREATE TABLE article(
  id int NOT NULL IDENTITY,
  user_id int NOT NULL,
  subcategory_id int NOT NULL,
  brand_id int DEFAULT NULL,
  article_name varchar(255) NOT NULL,
  article_price float DEFAULT NULL,
  article_date datetime2(0) NOT NULL,
  article_available_date datetime2(0) DEFAULT NULL,
  article_quantity int CHECK (article_quantity > 0) DEFAULT NULL,
  article_status smallint DEFAULT '1',
  article_support varchar(50) NOT NULL DEFAULT 'yes',
  article_description varchar(max),
  article_like int CHECK (article_like > 0) DEFAULT NULL,
  article_view bigint CHECK (article_view > 0) DEFAULT NULL,
  boost varchar(255) DEFAULT NULL,
  weight float DEFAULT NULL,
  lat varchar(255) DEFAULT NULL,
  lng varchar(255) DEFAULT NULL,
  type smallint DEFAULT NULL,
  PRIMARY KEY (id)
)  ;

CREATE INDEX IDX_23A0E665DC6FE57 ON article (subcategory_id);
CREATE INDEX IDX_23A0E6644F5D008 ON article (brand_id);
CREATE INDEX IDX_23A0E66A76ED395 ON article (user_id);


INSERT INTO article (id, user_id, subcategory_id, brand_id, article_name, article_price, article_date, article_available_date, article_quantity, article_status, article_support, article_description, article_like, article_view, boost, weight, lat, lng, type) VALUES
(5, 20, 6, 1, 'Apple Watch', NULL, '2022-08-09 11:32:10', '2022-08-09 00:00:00', NULL, 1, '1', 'Soon available on PLEAZIA', NULL, 8, NULL, NULL, '48.856614', '48.856614', NULL),
(7, 20, 6, 1, 'AirPods Pro', NULL, '2022-08-09 12:06:18', '2022-08-09 00:00:00', NULL, 1, '1', 'Soon available on PLEAZIA', 1, 6, NULL, NULL, '48.856614', '48.856614', NULL),
(9, 20, 6, 1, 'iPhone Pro', NULL, '2022-08-09 12:15:47', '2022-08-09 00:00:00', NULL, 1, '1', 'Soon available on PLEAZIA', 1, 7, NULL, NULL, '48.856614', '48.856614', NULL),
(11, 20, 6, 4, 'Bluetooth headphones', NULL, '2022-08-09 12:27:40', '2022-08-09 00:00:00', NULL, 1, '1', 'Soon available on PLEAZIA', 1, 15, NULL, NULL, '48.856614', '48.856614', NULL),
(12, 20, 5, 4, 'Apricots', NULL, '2022-08-09 12:47:57', '2022-08-09 00:00:00', NULL, 1, '1', 'Soon available on PLEAZIA', 1, 18, NULL, NULL, '48.856614', '48.856614', NULL),
(13, 20, 5, 4, 'Pineapple', NULL, '2022-08-09 15:37:37', '2022-08-09 00:00:00', NULL, 1, '1', 'Soon available on PLEAZIA', 1, 5, NULL, NULL, '48.856614', '48.856614', NULL),
(14, 20, 5, 4, 'Bananas', NULL, '2022-08-09 15:40:49', '2022-08-09 00:00:00', NULL, 1, '1', 'Soon available on PLEAZIA', 1, 9, NULL, NULL, '48.856614', '48.856614', NULL),
(15, 20, 5, 4, 'Coconut', NULL, '2022-08-09 15:43:24', '2022-08-09 00:00:00', NULL, 1, '1', 'Soon available on PLEAZIA', 1, 6, NULL, NULL, '48.856614', '48.856614', NULL),
(16, 20, 5, 4, 'Lemon', NULL, '2022-08-09 15:44:58', '2022-08-09 00:00:00', NULL, 1, '1', 'Soon available on PLEAZIA', 1, 6, NULL, NULL, '48.856614', '48.856614', NULL),
(17, 20, 5, 4, 'Melon', NULL, '2022-08-09 15:47:40', '2022-08-09 00:00:00', NULL, 1, '1', 'Soon available on PLEAZIA', 1, 31, NULL, NULL, '48.856614', '48.856614', NULL),
(18, 20, 6, 4, 'Smartwatch', NULL, '2022-08-09 16:24:49', '2022-08-09 00:00:00', NULL, 1, '1', 'Soon available on PLEAZIA', 1, 7, NULL, NULL, '48.856614', '48.856614', NULL),
(19, 20, 6, 2, 'Smartphone Galaxy', NULL, '2022-08-09 16:38:02', '2022-08-09 00:00:00', NULL, 1, '1', 'Soon available on PLEAZIA', 2, 117, NULL, NULL, '48.856614', '48.856614', NULL),
(20, 20, 9, 1, 'Breakfast sausage', NULL, '2022-08-09 22:22:31', '2022-08-10 00:00:00', NULL, 1, '1', 'Soon available on PLEAZIA', NULL, 38, NULL, NULL, '48.856614', '48.856614', NULL),
(21, 20, 10, 4, 'Cosmetic products', NULL, '2022-08-09 22:44:51', '2022-08-10 00:00:00', NULL, 1, '1', 'Soon available on PLEAZIA', 1, 6, NULL, NULL, '48.856614', '48.856614', NULL),
(22, 20, 10, 4, 'Apple cream', NULL, '2022-08-09 22:51:34', '2022-08-10 00:00:00', NULL, 1, '1', 'Soon available on PLEAZIA', NULL, 6, NULL, NULL, '48.856614', '48.856614', NULL),
(23, 20, 10, 4, 'Coconut cream', NULL, '2022-08-09 23:00:48', '2022-08-10 00:00:00', NULL, 1, '1', 'Soon available on PLEAZIA', 1, 6, NULL, NULL, '48.856614', '48.856614', NULL),
(24, 20, 10, 4, 'Lemon soap', NULL, '2022-08-09 23:08:14', '2022-08-10 00:00:00', NULL, 1, '1', 'Soon available on PLEAZIA', 1, 6, NULL, NULL, '48.856614', '48.856614', NULL),
(25, 20, 10, 4, 'Pink cream', NULL, '2022-08-09 23:20:25', '2022-08-10 00:00:00', NULL, 1, '1', 'Soon available on PLEAZIA', 1, 6, NULL, NULL, '48.856614', '48.856614', NULL),
(26, 20, 10, 4, 'Blue cream', NULL, '2022-08-09 23:24:57', '2022-08-10 00:00:00', NULL, 1, '1', 'Soon available on PLEAZIA', NULL, 6, NULL, NULL, '48.856614', '48.856614', NULL),
(27, 20, 11, 4, 'Yellow bodysuit', NULL, '2022-08-09 23:28:05', '2022-08-10 00:00:00', NULL, 1, '1', 'Soon available on PLEAZIA', 1, 6, NULL, NULL, '48.856614', '48.856614', NULL),
(28, 20, 11, 4, 'Sporty set', NULL, '2022-08-09 23:33:03', '2022-08-10 00:00:00', NULL, 1, '1', 'Soon available on PLEAZIA', 2, 6, NULL, NULL, '48.856614', '48.856614', NULL),
(29, 20, 11, 4, 'Woman jeans', NULL, '2022-08-09 23:53:59', '2022-08-10 00:00:00', NULL, 1, '1', 'Soon available on PLEAZIA', 2, 6, NULL, NULL, '48.856614', '48.856614', NULL),
(30, 20, 11, 4, 'Winter style', NULL, '2022-08-09 23:56:50', '2022-08-10 00:00:00', NULL, 1, '1', 'Soon available on PLEAZIA', 1, 6, NULL, NULL, '48.856614', '48.856614', NULL),
(31, 20, 11, 4, 'Set to wear', NULL, '2022-08-10 00:00:30', '2022-08-10 00:00:00', NULL, 1, '1', 'Soon available on PLEAZIA', NULL, 6, NULL, NULL, '48.856614', '48.856614', NULL),
(32, 20, 11, 4, 'Green dress', NULL, '2022-08-10 00:02:38', '2022-08-10 00:00:00', NULL, 1, '1', 'Soon available on PLEAZIA', NULL, 67, NULL, NULL, '48.856614', '48.856614', NULL),
(33, 20, 9, 4, 'Cheeseburger', NULL, '2022-08-10 10:56:46', '2022-08-10 00:00:00', NULL, 1, '1', 'Bientôt disponible sur PLEAZIA', 1, 6, NULL, NULL, '48.856614', '48.856614', NULL),
(34, 20, 9, 4, 'Hot dog sausages', NULL, '2022-08-10 11:01:57', '2022-08-10 00:00:00', NULL, 1, '1', 'Bientôt disponible sur PLEAZIA', 1, 13, NULL, NULL, '48.856614', '48.856614', NULL),
(35, 20, 9, 4, 'Beef pate', NULL, '2022-08-10 11:04:04', '2022-08-10 00:00:00', NULL, 1, '1', 'Bientôt disponible sur PLEAZIA', 1, 6, NULL, NULL, '48.856614', '48.856614', NULL),
(36, 20, 9, 4, 'Roast chicken', NULL, '2022-08-10 11:07:31', '2022-08-10 00:00:00', NULL, 1, '1', 'Bientôt disponible sur PLEAZIA', NULL, 7, NULL, NULL, '48.856614', '48.856614', NULL),
(37, 20, 9, 4, 'Pizza cheese', NULL, '2022-08-10 11:15:45', '2022-08-10 00:00:00', NULL, 1, '1', 'Bientôt disponible sur PLEAZIA', 2, 467, NULL, NULL, '48.856614', '48.856614', NULL);


ALTER TABLE article
  ADD CONSTRAINT FK_23A0E6644F5D008 FOREIGN KEY (brand_id) REFERENCES brand (id);


